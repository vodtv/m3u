<div align="center">
<img src="https://vodtv.gitee.io/img/iptv/logo.png" height="200"/>
<h1 align="center">✯ M3U - VODTV ✯</h1>
<h3>收集来自世界各地的公开可用的IPTV频道支持IPv4/IPv6双栈访问 </h3>
<h3>🔗 https://m3u.vodtv.cn</h3>
</div>

---
## 目录

- ❓  [如何使用](#如何使用)
- 📺 [播放列表](#播放列表)
- 🚀 [加速工具](#加速工具)
- 🆕 [当前更新](#当前更新)
- 📖 [免责申明](#免责申明)

## 如何使用

只需将以下链接之一插入任何支持实时流式传输的视频播放器，然后按打开即可。

## 播放列表

| channel | url | list | count |
| ------- | --- | ---- | ----- |

<!-- channels_here -->

## 加速工具

### 本站 m3u.vodtv.cn

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://m3u.vodtv.cn/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://m3u.vodtv.cn/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://m3u.vodtv.cn/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://m3u.vodtv.cn/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 镜像raw.gitcode.com （备用）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://raw.gitcode.com/iptv/m3u/raw/gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://raw.gitcode.com/iptv/m3u/raw/gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://raw.gitcode.com/iptv/m3u/raw/gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://raw.gitcode.com/iptv/m3u/raw/gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 镜像raw.gitlink.org.cn （备用）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://www.gitlink.org.cn/api/iptv/m3u/raw/cn.m3u?ref=gh-pages</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://www.gitlink.org.cn/api/iptv/m3u/raw/txt/cn.txt?ref=gh-pages</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://www.gitlink.org.cn/api/iptv/m3u/raw/epg/51zmt.xml?ref=gh-pages</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://www.gitlink.org.cn/api/iptv/m3u/raw/sources/cn.json?ref=gh-pages</td>
    </tr>
  </tbody>
</table>

### 镜像cdn.jsdelivr.net （备用）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://cdn.jsdelivr.net/gh/vodtv/iptv@gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://cdn.jsdelivr.net/gh/vodtv/iptv@gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://cdn.jsdelivr.net/gh/vodtv/iptv@gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://cdn.jsdelivr.net/gh/vodtv/iptv@gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 镜像cdn.gitmirror.com （备用）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://cdn.gitmirror.com/gh/vodtv/iptv@gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 镜像cdn.statically.io （备用）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>M3U</td>
      <td>https://cdn.statically.io/gh/vodtv/iptv@gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://cdn.statically.io/gh/vodtv/iptv@gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://cdn.statically.io/gh/vodtv/iptv@gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://cdn.statically.io/gh/vodtv/iptv@gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

### 原网址前加ghproxy.net （备用）

<table>
  <thead>
    <tr>
      <th>格式</th>
      <th>示例 </th>
    </tr>
  </thead>
   <tbody>
    <tr>
      <td>M3U</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/cn.m3u</td>
    </tr>
     <tr>
      <td>TXT</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/txt/cn.txt</td>
    </tr>
    <tr>
      <td>XML</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/epg/51zmt.xml</td>
    </tr>
    <tr>
      <td>JSON</td>
      <td>https://ghproxy.net/https://raw.githubusercontent.com/vodtv/iptv/gh-pages/sources/cn.json</td>
    </tr>
  </tbody>
</table>

## 当前更新

 <!-- updated_here -->

## 免责申明

- 所有播放源均收集于互联网，仅供测试研究使用，不得商用。
- 通过 M3U8 Web Player 测试直播源需使用 https 协议的直播源链接。
- 部分广播电台节目播出具有一定的时效性，需要在指定时段进行收听。
- 本项目不存储任何的流媒体内容，所有的法律责任与后果应由使用者自行承担。
- 您可以 Fork 本项目，但引用本项目内容到其他仓库的情况，务必要遵守开源协议。
- 本项目不保证直播频道的有效性，直播内容可能受直播服务提供商因素影响而失效。
